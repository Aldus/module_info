<?php

declare(strict_types=1);

/**
 *
 *  @module         Module Information
 *  @version        see info.php of this module
 *  @author         Dietrich Roland Pehlke
 *  @copyright      2009-2023 Dietrich Roland Pehlke
 *  @license        http://www.gnu.org/licenses/gpl.html
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 */

class module_info extends LEPTON_abstract
{
    const TABLES = [
        'info' => TABLE_PREFIX ."mod_module_info"
    ];

    const PLATFORM_VERSIONS = [
        'LEPTON 1.x',
        'LEPTON 2.x',
        'LEPTON 1.x/2.x hybrid',
        'LEPTON 3.x',
        'LEPTON IV',
        'LEPTON V',
        'LEPTON VI',
        'LEPTON VII',
        'LEPTON VIII'
    ];
    
    // Own instance for this class!
    static $instance;

    protected function initialize()
    {
        // TODO: Implement initialize() method.
    }

    /**
     *  Convert some special chars to their js-pendant.
     *
     * @param   mixed   $aStr   A given var.
     * @return  mixed
     */
    public function restore_string(mixed $aStr = ""): mixed
    {
        if (is_string($aStr))
        {
            $lookup = [
                "<br />" => "\n",
                "\\'" => "'",
                "\\\"" => "\""
            ];
            return str_replace(array_keys($lookup), array_values($lookup), ($aStr ?? ""));
        } else {
            return $aStr;
        }
    }
}
