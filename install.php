<?php

/**
 *
 *  @module         Module Information
 *  @version        see info.php of this module
 *  @author         Dietrich Roland Pehlke
 *  @copyright      2009-2023 Dietrich Roland Pehlke
 *  @license        http://www.gnu.org/licenses/gpl.html
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$database = LEPTON_database::getInstance();
$table = TABLE_PREFIX . "mod_module_info";

$queries = [];

$queries[] = "DROP TABLE IF EXISTS ".$table;

$query = "CREATE TABLE `" . $table . "` (
    `section_id`    int(11) NOT NULL,
    `page_id`       int(11) NOT NULL,
    `modul`         varchar(255) DEFAULT 'no_name',
    `type`          varchar(255) DEFAULT 'page',
    `license`       varchar(255) DEFAULT 'GPL',
    `wb_name`       varchar(255) DEFAULT 'no_name',
    `author`        varchar(255) DEFAULT 'no_name',
    `contact`       varchar(255) DEFAULT '',
    `version`       varchar(255) DEFAULT '0.1.0',
    `state`         varchar(255) DEFAULT 'dev',
    `see_also`      varchar(255) DEFAULT '',
    `last_info`     date DEFAULT NULL,
    `download`      varchar(255) DEFAULT NULL,
    `wb_thread`     varchar(255) DEFAULT NULL,
    `web_link`      varchar(255) DEFAULT NULL,
    `screen`        varchar(255) DEFAULT NULL,
    `description`   text,
    `guid`          varchar(36) NOT NULL DEFAULT '',
    `platform`      varchar(64) NOT NULL DEFAULT '4.1',
    `group`         varchar(255) NOT NULL DEFAULT '',
    `requires`      varchar(255) NOT NULL DEFAULT '',
    `counter`       int(11) NOT NULL DEFAULT '0',
    `rating`        varchar(255) NOT NULL DEFAULT '',
    `votes`         int(11) NOT NULL DEFAULT '0',
    `info`          text,
    PRIMARY KEY (`section_id`)
);";

$queries[] = $query;

$queries[] = "DROP TABLE IF EXISTS ".$table."_images";

$query = "CREATE TABLE `" . $table . "_images` (
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT,
    `active`        int(1)  unsigned DEFAULT '0',
    `page_id`       int(11) unsigned DEFAULT NULL,
    `section_id`    int(11)	unsigned DEFAULT NULL,
    `position`      int(10) unsigned NOT NULL DEFAULT '1',
    `src`           tinytext,
    `title`         varchar(255) DEFAULT NULL,
    `alt`           varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);";

$queries[] = $query;

foreach ($queries as &$q)
{
    $database->execute_query($q);
}
