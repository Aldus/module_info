/**
 *
 *  @module         Module Information
 *  @version        see info.php of this module
 *  @author         Dietrich Roland Pehlke
 *  @copyright      2009-2023 Dietrich Roland Pehlke
 *  @license        http://www.gnu.org/licenses/gpl.html
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 */

var url = window.location.pathname;
var filename = url.substring(url.lastIndexOf('/')+1);

if (filename != "save.php") {
    $('.menu .item')
      .tab()
    ;
}
