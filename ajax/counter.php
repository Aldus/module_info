<?php

declare(strict_types=1);

/**
 *
 *  @module         Module Information
 *  @version        see info.php of this module
 *  @author         Dietrich Roland Pehlke
 *  @copyright      2009-2023 Dietrich Roland Pehlke
 *  @license        http://www.gnu.org/licenses/gpl.html
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

if (!isset($_SESSION['mih_']))
{
    die();
}

$h = $_SESSION['mih_'];
$n = substr($h, 0, 16);
$v = substr($h, -16);
if (!isset($_POST[$n]))
{
    die();
}

if ($_POST[$n] != $v)
{
    die();
}

unset($_SESSION['mih_']);

$result = filter_input(
    INPUT_POST,
    "job",
    FILTER_VALIDATE_REGEXP,
    [ "options"     => [
        "regexp"    => "~dl~",
        "default"   => NULL
    ]]
);

if (isset($_POST['job']))
{
    switch ($_POST['job'])
    {
		case 'dl':
			$id = intval($_POST['id']);

			LEPTON_database::getInstance()->execute_query(
                "UPDATE `" . TABLE_PREFIX . "mod_module_info` set `counter`=`counter`+1 WHERE `section_id`='" . $id . "'"
			);
            echo "ok " . $id;
			break;

        default:
            echo "0";
	}
} else {
    echo "-1";
}

