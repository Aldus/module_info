<?php

/**
 *
 *  @module         Module Information
 *  @version        see info.php of this module
 *  @author         Dietrich Roland Pehlke
 *  @copyright      2009-2023 Dietrich Roland Pehlke
 *  @license        http://www.gnu.org/licenses/gpl.html
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$mod_headers = array(
    'frontend' => array(
        'css' => array(
            array(
                'media' => 'screen',
                'file'  => 'modules/lib_jquery/plugins/colorbox/colorbox.css',
            ),
            array(
                'media' => 'screen',
                'file'  => 'modules/module_info/rating/rating.css'
            )
        ),
        'js' => array(
            'modules/lib_jquery/jquery-core/jquery-core.min.js',
            'modules/lib_jquery/jquery-core/jquery-migrate.min.js',
            'modules/lib_jquery/plugins/colorbox/jquery.colorbox-min.js'
        )
    ),
    'backend' => array(
        'css' => array(
            array(
                'media' => 'all',
                'file'  => 'modules/lib_fomantic/dist/semantic.min.css'
                ),
            array(
                'media' => 'screen',
                'file'  => 'modules/lib_jquery/plugins/colorbox/colorbox.css',
            )
        ),
        'js' => array(
            'modules/lib_jquery/jquery-core/jquery-core.min.js',
            'modules/lib_jquery/jquery-core/jquery-migrate.min.js',
            'modules/lib_fomantic/dist/semantic.min.js',
            'modules/lib_jquery/plugins/colorbox/jquery.colorbox-min.js',
            'modules/module_info/js/jquery.dragsort-0.5.2.min.js'
        )
    )
);

