<?php

/**
 *
 *  @module         Module Information
 *  @version        see info.php of this module
 *  @author         Dietrich Roland Pehlke
 *  @copyright      2009-2023 Dietrich Roland Pehlke
 *  @license        http://www.gnu.org/licenses/gpl.html
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 */

/**
 *  Variables for the backend and the frontend
 */
$MOD_MODULE_INFO = [
	'MODUL'   =>   'Module',
	'TYPE'    =>   'Type',
	'AUTHOR'  =>  'Author',
	'WB_NAME' =>  'Nick',
	'CONTACT' =>  'Support',
	'VERSION' =>  'Version',
	'STATE'   =>   'State',
	'LICENSE' =>  'License',
	'DOWNLOAD'    =>   'Download',
	'WB_THREAD'   =>   'Forum',
	'WEB_LINK'    =>   'Web Link',
	'DESCRIPTION' =>  'Description',
	'INFO'        =>  'Info',
	'SEE_ALSO'    =>   'See also',
	'LAST_INFO'   =>   'Last info',
	'AMASP_UPLOAD'    =>   'LEPTON Upload',
	'SCREEN'  =>  'Image',
	'RATING'  =>  'Rating',
	'SCREEN_INFO' =>  '<span style=\'color: #880000;\'>Bitte nicht mehr als 500 Pixel Breite!</span>',
	'GUID'        =>  'GUID',
	'PLATFORM'    =>   'Platform',
	'REQUIRES'    =>   'Requires',
	'REQUIRES_DISPLAY' =>  '<span class="require">%s</span>',
	'LABEL_BLOCK_A'    =>   'Basisdaten',
	'LABEL_BLOCK_B'    =>   'Beschreibungen',
	'LABEL_BLOCK_C'    =>   'Bilder',
	'LABEL_BLOCK_D'    =>   'Statistik',
	'LABEL_STATS_DOWNLOAD' =>  'Downloads',
	'LABEL_STATS_VOTES'    =>   'Stimmen',
	'LABEL_STATS_AVERAGE'  =>  'Average',
	'DISPLAY_EXTERNAL_LINK' =>  '<a href="%s" target="_blank">%s</a>',
];
