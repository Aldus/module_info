<?php

declare(strict_types=1);

/**
 *
 *  @module         Module Information
 *  @version        see info.php of this module
 *  @author         Dietrich Roland Pehlke
 *  @copyright      2009-2023 Dietrich Roland Pehlke
 *  @license        http://www.gnu.org/licenses/gpl.html
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 */
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

$database = LEPTON_database::getInstance();

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule("module_info");

$oMODULE_INFO = module_info::getInstance();
$MOD_MODULE_INFO = $oMODULE_INFO->language;

/**
 *	Here we go
 */
$table = TABLE_PREFIX."mod_module_info";

$data = [];
$database->execute_query(
    "SELECT * FROM `".$table."` WHERE section_id=".$section_id,
    true,
    $data,
    false
);

foreach ($data as $k=>$v)
{
    $data[$k] = $oMODULE_INFO->restore_string($v);
}

$state_options = ['Alpha', 'Beta', 'RC', 'Stable'];

$state_select = "<select name='state' class='mod_info' >\n";
foreach ($state_options as $i)
{
    $state_select .= "<option value='".$i."' ".($i==$data['state'] ? 'selected="selected"':'').">".$i."</option>";
}
$state_select .= "</select>";

$page_types = ["page", "template", "admin tool", "snippet", "code", "core replacement", "BE-Theme", "wysiwyg", "library"];
$type_select = "<select name='type' class='mod_info' >\n";
foreach ($page_types as $i)
{
    $type_select .= "<option value=\"".$i."\" ".($i==$data['type'] ? 'selected="selected"' : '' ).">".$i."</option>\n";
}
$type_select .= "</select>\n";

/**
 *	Image/Screen preview
 */
$screen_preview = "";
$images = [];
$database->execute_query(
	"SELECT `id`,`active`,`src`,`title`,`alt` FROM `".TABLE_PREFIX."mod_module_info_images` WHERE `section_id`='".$section_id."' ORDER BY `position`",
	true,
	$images
);

if (!empty($images))
{
	$screen_preview = $oTWIG->render(
		"@module_info/images_details.lte",
		[
			'images' => $images,
			'THEME_URL'	=> THEME_URL,
			'LEPTON_URL' => LEPTON_URL,
			'section_id' => $section_id
		]
	);
}
/**
 *	Platform
 *
 */
$platform_select = "<select id='platform_select' name='platform' class='mod_info' >\n";
foreach (module_info::PLATFORM_VERSIONS as $temp)
{
    $platform_select .= sprintf(
        "<option value='%s' %s>%s</option>\n",
        $temp,
        ($data['platform']==$temp? "selected='selected' ":''),
        $temp
    );
}
$platform_select .= "</select>\n";

/**
 *	Parent, Page_ID
 *
 *
 */

/**
 *	JS File uploader
 */
$js_source = file_get_contents( __DIR__."/js/backend_upload.js");
$js_source = str_replace(
	array("{LEPTON_URL}", "{THEME_URL}", "{SECTION_ID}"),
	array( LEPTON_URL, THEME_URL, $section_id ),
	$js_source
);

echo $js_source;

/**
 *	Prepare all information
 *
 */
$module_data = array(
	
	'block_a' => array(
		'label' => $MOD_MODULE_INFO['LABEL_BLOCK_A'],
		'items'	=> array()
		),
	
	'block_b' => array(
		'label' => $MOD_MODULE_INFO['LABEL_BLOCK_B'],
		'items' => array()
		),
		
	'block_c' => array(
		'label' => $MOD_MODULE_INFO['LABEL_BLOCK_C'],
		'items' => array()
		),
	
	'block_d' => array(
		'label' => $MOD_MODULE_INFO['LABEL_BLOCK_D'],
		'items' => array()
		)
);

//	Name of the module
$module_data['block_a']['items'][] = array(
	'type'	=> "text",
	'name'	=> "modul",
	'label' => $MOD_MODULE_INFO['MODUL'],
	'value'	=> $data['modul']
);

//	Type of the module
$module_data['block_a']['items'][] = array(
	'type'	=> 'select',
	'name'	=> 'select',
	'label' => $MOD_MODULE_INFO['TYPE'],
	'value'	=> $type_select	// #1
);

//	Author of the module
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'author',
	'label' => $MOD_MODULE_INFO['AUTHOR'],
	'value'	=> $data['author']
);

//	WB_name - Nick-name
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'wb_name',
	'label' => $MOD_MODULE_INFO['WB_NAME'],
	'value'	=> $data['wb_name']
);

//	Contact information
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'contact',
	'label' => $MOD_MODULE_INFO['CONTACT'],
	'value'	=> $data['contact']
);

//	Version
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'version',
	'label' => $MOD_MODULE_INFO['VERSION'],
	'value'	=> $data['version']
);

//	State
$module_data['block_a']['items'][] = array(
	'type'	=> 'select',
	'name'	=> 'state',
	'label' => $MOD_MODULE_INFO['STATE'],
	'value'	=> $state_select
);

//	license
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'license',
	'label' => $MOD_MODULE_INFO['LICENSE'],
	'value'	=> $data['license']
);

//	GUID
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'guid',
    'label' => ($data['guid'] == "" ? "<a href='https://guidgenerator.com/' target='blank'>(get guid)</a> " : "") . $MOD_MODULE_INFO['GUID'],
	'value'	=> $data['guid']
);

//	platform
$module_data['block_a']['items'][] = array(
	'type'	=> 'select',
	'name'	=> 'platform',
	'label' => $MOD_MODULE_INFO['PLATFORM'],
	'value'	=> $platform_select
);

//	download - text/url
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'download',
	'label' => $MOD_MODULE_INFO['DOWNLOAD'],
	'value'	=> $data['download']
);

//	amasp_upload
$module_data['block_a']['items'][] = array(
	'type'	=> 'file',
	'name'	=> 'amasp_upload',
	'label' => $MOD_MODULE_INFO['AMASP_UPLOAD'],
	'value'	=> ""
);

//	wb_thread
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'wb_thread',
	'label' => $MOD_MODULE_INFO['WB_THREAD'],
	'value'	=> $data['wb_thread']
);

//	web_link
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'web_link',
	'label' => $MOD_MODULE_INFO['WEB_LINK'],
	'value'	=> $data['web_link']
);

//	see_also
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'see_also',
	'label' => $MOD_MODULE_INFO['SEE_ALSO'],
	'value'	=> $data['see_also']
);

//	requires
$module_data['block_a']['items'][] = array(
	'type'	=> 'text',
	'name'	=> 'requires',
	'label' => $MOD_MODULE_INFO['REQUIRES'],
	'value'	=> $data['requires']
);

//	######## Block b ######################
//	description
$module_data['block_b']['items'][] = array(
	'type'	=> 'textarea',
	'name'	=> 'description',
	'label' => $MOD_MODULE_INFO['DESCRIPTION'],
	'value'	=> $data['description']
);

//	info
$module_data['block_b']['items'][] = array(
	'type'	=> 'textarea',
	'name'	=> 'info',
	'label' => $MOD_MODULE_INFO['INFO'],
	'value'	=> $data['info']
);

//	######### Block c #####################
//	screen
$module_data['block_c']['items'][] = array(
	'type'	=> 'file_screen',
	'name'	=> 'screen',
	'label' => $MOD_MODULE_INFO['SCREEN'],
	'value'	=> $screen_preview
);

//	######### Block d ######################
//	Download statistics
$module_data['block_d']['items'][] = array(
	'type'	=> 'html',
	'name'	=> 'downloads',
	'label' => $MOD_MODULE_INFO['LABEL_STATS_DOWNLOAD'],
	'value'	=> intval($data['counter'])
);

//	Votes
$module_data['block_d']['items'][] = array(
	'type'	=> 'html',
	'name'	=> 'votes',
	'label' => $MOD_MODULE_INFO['LABEL_STATS_VOTES'],
	'value'	=> intval($data['votes'])
);

//	Average
if ($data['rating']=="")
{
    $data['rating']="0,0,0,0,0,0";
}
$temp = explode(",", $data['rating']);
$n = 0;
if (count($temp) < 6) {
	for($i = count($temp); $i<6; $i++) $temp[] = 0;
}
for($i=1;$i<=5;$i++) $n += $temp[$i]*$i;

$av = ((int)$data['votes'] == 0)
	? 0
	: ($n / ((int)$data['votes']*5)) * 100
	;

$module_data['block_d']['items'][] = array(
	'type'	=> 'html',
	'name'	=> 'average',
	'label' => $MOD_MODULE_INFO['LABEL_STATS_AVERAGE'],
	'value'	=> $av."%"
);

/** ***********
 *	Form values
 */
$form_values = array(
	'LEPTON_URL'	=> LEPTON_URL,
	'page_id'	=> $page_id,
	'section_id'	=> $section_id,
	'module_data' => $module_data
);

   
echo $oTWIG->render(
	"@module_info/modify.lte",
	$form_values
);

