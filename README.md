### Module Information

A page module for [LEPTON CMS][1]
This modul allows to keep information about a module, code-snippet or template/backend-theme.

#### Requirements
* PHP >= 8.0 (8.2 recommended)
* [LEPTON CMS][1] >= 7

[1]: https://lepton-cms.org "LEPTON CMS"