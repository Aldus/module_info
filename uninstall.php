<?php

/**
 *
 *  @module         Module Information
 *  @version        see info.php of this module
 *  @author         Dietrich Roland Pehlke
 *  @copyright      2009-2023 Dietrich Roland Pehlke
 *  @license        http://www.gnu.org/licenses/gpl.html
 *  @license terms  see info.php of this module
 *  @platform       see info.php of this module
 *
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php
/**
 *	Drop tables.
 */
$table = TABLE_PREFIX ."mod_module_info";
$database->query("DROP TABLE ".$table );
$database->query("DROP TABLE ".$table."_images" );

/**
 *	Remove folders and images inside the media-directory.
 */
if (!function_exists("rm_full_dir")) {
	require_once( LEPTON_PATH."/framework/functions/function.rm_full_dir.php");
}

$filename = LEPTON_PATH.MEDIA_DIRECTORY."/lepton_uploads/";
if (file_exists($filename)) rm_full_dir( $filename );

$filename = LEPTON_PATH.MEDIA_DIRECTORY."/lepton_screen_uploads/";
if (file_exists($filename)) rm_full_dir( $filename );

?>